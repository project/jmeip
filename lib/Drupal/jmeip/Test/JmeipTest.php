<?php

/**
 * @file
 * Contains Drupal\minimal\Tests\MinimalTest.
 */

namespace Drupal\jmeip\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Tests Just my essential installation profile expectations.
 */
class JmeipTest extends WebTestBase {

  protected $profile = 'jmeip';

  public static function getInfo() {
    return array(
      'name' => 'Just my essential installation profile',
      'description' => 'Tests Just my essential installation profile expectations.',
      'group' => 'JMEIP',
    );
  }

  /**
   * Tests Just my essential installation profile.
   */
  function testJmeip() {
    $this->drupalGet('');
    // Check the login block is present.
    // $this->assertLink(t('Create new account'));
    // $this->assertResponse(200);

    // Create a user to test tools and navigation blocks for logged in users
    // with appropriate permissions.
    $user = $this->drupalCreateUser(array('access administration pages', 'administer content types'));
    $this->drupalLogin($user);
    $this->drupalGet('');
    $this->assertText(t('Tools'));
    $this->assertText(t('Administration'));
  }
}
