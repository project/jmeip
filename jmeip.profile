<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Symfony\Component\Yaml\Parser;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function jmeip_form_install_configure_form_alter(&$form, $form_state) {
  $yaml = new Parser();
  $info = $yaml->parse(file_get_contents(drupal_get_path('profile', 'jmeip') . '/content/site_info.yml'));

  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = (isset($info['site']['name'])) ? $info['site']['name'] : $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#default_value'] = (isset($info['site']['email'])) ? $info['site']['email'] : 'noreply@' . $_SERVER['SERVER_NAME'] . '.com';

  $form['admin_account']['account']['name']['#default_value'] = (isset($info['userone']['name'])) ? $info['userone']['name'] : 'rootiovox';
  $form['admin_account']['account']['mail']['#default_value'] = (isset($info['userone']['email'])) ? $info['userone']['email'] : $form['admin_account']['account']['name']['#default_value'] . '@' . $_SERVER['SERVER_NAME'] . '.com';

  $form['regional_settings']['site_default_country']['#default_value'] = (isset($info['server']['country'])) ? $info['server']['country'] : 'US';
  $form['regional_settings']['date_default_timezone']['#default_value'] = (isset($info['server']['timezone'])) ? $info['server']['timezone'] : 'America/Los_Angeles';

  if(isset($info['server']['timezone'])
  && isset($form['server_settings']['date_default_timezone']['#attributes']['class'])) {
    unset($form['server_settings']['date_default_timezone']['#attributes']['class']);
    $form['server_settings']['date_default_timezone']['#attributes']['class'] = array();
  }

  $form['update_notifications']['update_status_module']['#default_value'][1] = (isset($info['update']['check'])) ? $info['update']['check'] : 1;
  $form['update_notifications']['update_status_module']['#default_value'][0] = (isset($info['update']['email'])) ? $info['update']['email'] : 0;

}
